<?php
return [
	'register' => 'site/register',
	'login' => 'site/login',
	'logout' => 'site/logout',
	'note/(\d+)' => 'note/show/$1',
	'note/update/(\d+)' => 'note/update/$1',
	'note/add' => 'note/add',
	'site/sort/(\w+)' => 'site/sort/$1',
	'delete/(\d+)' => 'note/delete/$1',
	'' => 'site/index',
];
