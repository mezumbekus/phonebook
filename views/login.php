 <?php include ROOT.'/views/header.php' ?>
 <div class="container">

      <form class="form-signin" action="/login" method="post">
        <h2 class="form-signin-heading">Авторизация</h2>
        <input type="text" class="form-control" placeholder="Login" required autofocus name="login">
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <p><?php echo "$captcha" ?></p>
        <input type="text" class="form-control" name="captcha" required>
        <a href="/register">Регистрация</a>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
        
      </form>
      <ul class="error-list">
      <?php 
      if(!empty($errors)){
      foreach($errors as $val){
         echo "<li>----$val----</li>";
      } 
      }
      ?>
      </ul>

</div>
 <?php include ROOT.'/views/footer.php' ?>