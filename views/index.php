<?php include_once ROOT.'/views/header.php' ?>
<nav class="navbar navbar-dark bg-dark">
  <span class="navbar-brand mb-0 h1">Телефонная книга 0.1</span>
  <?php if($loginId !== null): ?>
    <a class="navbar-brand col-mb-2 col-mb-offset-9" href="/logout">Выйти</a>
  <?php else: ?>
    <a class="navbar-brand col-mb-2 col-mb-offset-9" href="/login">Войти</a>
  <?php endif; ?>
  <?php if (isset($loginId)): ?>
   <a href="add" class="btn btn-primary note-add">Добавить запись</a>
  <?php endif; ?>
</nav>
<div class="notes-container col-lg-12">
<div class="row">
    <div class="col-lg-1 col-lg-offset-1">
        <h3>Телефон</h3>
        
    </div>
    <div class="col-lg-1">
        <h3><a href="/site/sort/name" class="note-sort-name">Имя</a></h3>
    </div>
    <div class="col-lg-1">
        <h3><a href="/site/sort/surname"  class="note-sort-surname">Фамилия</a></h3>
    </div>
    <div class="col-lg-2">
        <h3><a href="/site/sort/email"  class="note-sort-email">e-mail</a></h3>
    </div>
    <div class="col-lg-2">
        <h3>Комментарий</h3>
    </div>
    <div class="col-lg-2">
        <h3>Изображение</h3>
    </div>
    <div class="col-lg-1">
        <h3>Действие</h3>
    </div>
</div>
<div class="inner-container col-lg-12">
        <div class="_flag"></div>
        <?php if(!empty($list)) {
  foreach ($list as $note) {
?>  

<div class="row">
    <div class="col-lg-1 col-lg-offset-1">
        <p class='note-field-data' data-type='phone'><?php echo $note['phone'] ?></p>
        
    </div>
    <div class="col-lg-1">
        <p class='note-field-data' data-type='name'><?php echo $note['name']; ?></p>
    </div>
    <div class="col-lg-1">
        <p class='note-field-data' data-type='surname'><?php echo $note['surname']; ?></p>
    </div>
    <div class="col-lg-2">
        <p class='note-field-data' data-type='email' ><?php echo $note['email'] ?></p>
    </div>
    <div class="col-lg-2">
        <p class='note-field-data' data-type='comment'><?php echo $note['comment'] ?></p>
    </div>
    <div class="col-lg-2">
        <a href="<?php echo Helper::UPLOAD_DIR.$note['image'] ?>"><img class="note-image" src="<?php echo Helper::UPLOAD_DIR.$note['image'] ?>"></a>
    </div>
    <div class="col-lg-1">
        <ul>
          <li><a href="/edit/<?php echo $note['noteId'] ?>" class='note-edit-btn' data-id = "<?php echo $note['noteId'] ?>">Редактировать</a></li>
          <li><a href="/delete/<?php echo $note['noteId'] ?>" class='note-delete-btn'>Удалить</a></li>
        </ul>
    </div>

</div>


<?php
  }

}

?>
</div>

</div>


<?php include_once ROOT.'/views/footer.php' ?>