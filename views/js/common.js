
$(document).ready(function(){
var el = document.querySelector('.note-add');
var container = document.querySelector('.notes-container');
var popup = null;
var temp_el = null;
if(el){
el.onclick = function(e) {
        if(popup === null){
            popup = document.createElement('div');
            container.appendChild(popup);
        }
        popup.innerHTML = createPopup('new');
	e.preventDefault();
};
}
container.onclick = function(e) {
	
	if(e.target.classList.contains('note-popup-close')) {
                
		container.removeChild(popup);
                popup = null;
	}
	else if(e.target.classList.contains('note-send-btn')) {
	e.preventDefault();
	var formData = new FormData(popup.querySelector('.form-note'));
	       sendSortAjax('/note/add/','',formData);
        }
        else if(e.target.classList.contains('note-edit-btn')) {
            e.preventDefault();
            var objs = {};
            var parent = findParent(e.target,'row');
            var els = parent.querySelectorAll('.note-field-data');
            els = Array.prototype.slice.call(els);
           
            els.forEach(function(item){
            objs[item.dataset.type] = item.innerText;
            });
            objs['id'] = e.target.dataset.id;
            
            if(popup != null) {
             popup.innerHTML = createPopup('update',objs); 
            
            }  else {
            console.dir(objs);
            popup = document.createElement('div');
            container.appendChild(popup);
            
            popup.innerHTML = createPopup('update',objs);
            
            temp_el = findParent(e.target,'row');
            }
           
        }
    else if(e.target.classList.contains('note-update-btn')) {
        e.preventDefault();
        var formData = new FormData(popup.querySelector('.form-note'));
    
	    sendSortAjax('/note/update/'+e.target.dataset.id,'',formData);
	  
        
    
    }

    else if(e.target.classList.contains('note-sort-name')){
        e.preventDefault();
        //simpleSort('name');
        sendSortAjax('/site/sort/','1')
        
    }
    else if(e.target.classList.contains('note-sort-surname')){
        e.preventDefault();
        sendSortAjax('/site/sort/','2');
        
    }
    else if(e.target.classList.contains('note-sort-email')){
        e.preventDefault();
        sendSortAjax('/site/sort/','3');
        
    }
   
    
};


function sendSortAjax(url,type,formData) {
    var container = document.querySelector('.inner-container');
    var xhr = new XMLHttpRequest();
        xhr.open('POST', url+type);
        xhr.onload = function () {
            if (xhr.status === 200) {
                container.innerHTML = xhr.responseText;
                 
            } else {
                alert('Something went terribly wrong...');
            }
        
        };
        if(formData)
            xhr.send(formData);
        else xhr.send();
}

function simpleSort(type) {
    var container = document.querySelector('.inner-container');
    var items = [];
    var nodeList =  container.querySelectorAll('.row');
    for(var i = 0; i < nodeList.length; i++){
        if(nodeList[i].nodeType == 1){
            items.push(nodeList[i].parentElement.removeChild(nodeList[i]));
        }
    }

    items.sort(function(node1,node2){
        var a = node1.querySelector('P[data-type='+type+']').dataset[type];
        var b = node2.querySelector('P[data-type='+type+']').dataset[type];
        return (a < b) ? -1 : (a > b) ? 1 : 0; 

    }).forEach(function(item){
        container.appendChild(item);
    });
}

function findParent(el,_class) {
    while(!el.classList.contains(_class))
        el = el.parentElement;
    return el;
}


//type должен быть new или update
function createPopup(type,obj) {
    fields = {
        phone : '',
        name : '',
        surname : '',
        comment : '',
        email : '',
        id: ''
    };
    if(obj != undefined) {
       for(var k in obj) 
        if(obj[k])
            fields[k] = obj[k];
    }
    var data_id = '';
    switch(type){
        case 'new':
            var _class = 'note-send-btn';
            var _action = '/note/add';
            var title = 'Добавление записи';
            break;
        case 'update':
            var _class = 'note-update-btn';
            var _action = 'note/update/' + fields['id'];
            var title = 'Редактирование записи';
            data_id = "data-id='"+fields['id']+"'";
            break;
    }

 
    return "<div class='note-add-popup'> <form action='"+_action+"' method='post' enctype='multipart/form-data' class='form-note'>\
                        <h3>"+title+"</h3>\
                        <p>Телефон</p><input type='text' name='phone' value='"+fields['phone']+"'>\
                        <p>Имя</p><input type='text' name='name' value='"+fields['name']+"'>\
                        <p>Фамилия</p><input name='surname' type='text' value='"+fields['surname']+"'>\
                        <p>Комментарий</p><textarea name='comment'>"+fields['comment']+"</textarea>\
                        <p>Email</p><input name='email' type='text' value='"+fields['email']+"'>\
                        <p>Аватар</p><input name='image' type='file' class='note-photo'>\
                        <button  type='submit' class='"+_class+"' "+data_id+">Отправить</button>\
                       </form>\
                       <div class ='note-popup-close'>X</div>\
                       </div>";
}




});