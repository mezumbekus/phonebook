<?php if(!empty($list)) {
  foreach ($list as $note) {
?>  

<div class="row">
    
    <div class="col-lg-1 col-lg-offset-1">
        <p class='note-field-data' data-type='phone'><?php echo $note['phone'] ?></p>
        
    </div>
    <div class="col-lg-1">
        <p class='note-field-data' data-type='name'><?php echo $note['name']; ?></p>
    </div>
    <div class="col-lg-1">
        <p class='note-field-data' data-type='surname'><?php echo $note['surname']; ?></p>
    </div>
    <div class="col-lg-2">
        <p class='note-field-data' data-type='email' ><?php echo $note['email'] ?></p>
    </div>
    <div class="col-lg-2">
        <p class='note-field-data' data-type='comment'><?php echo $note['comment'] ?></p>
    </div>
    <div class="col-lg-2">
        <a href="<?php echo Helper::UPLOAD_DIR.$note['image'] ?>"><img class="note-image" src="<?php echo Helper::UPLOAD_DIR.$note['image'] ?>"></a>
    </div>
    <div class="col-lg-1">
        <ul>
          <li><a href="/edit/<?php echo $note['noteId'] ?>" class='note-edit-btn' data-id = "<?php echo $note['noteId'] ?>">Редактировать</a></li>
          <li><a href="/delete/<?php echo $note['noteId'] ?>" class='note-delete-btn'>Удалить</a></li>
        </ul>
    </div>

</div>


<?php
  }

}

?>