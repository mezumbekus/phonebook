 <?php include ROOT.'/views/header.php' ?>
 <div class="container">

      <form class="form-signin" action="/register" method="post">
        <h2 class="form-signin-heading">Регистрация</h2>
        <input type="text" class="form-control" name="login" placeholder="Логин" required autofocus>
        <input type="password" class="form-control" name="password" placeholder="Пароль" required>
        <input type="password" class="form-control" name="password2" placeholder="Повторите пароль" required>
        <input type="email" class="form-control" name="email" placeholder="Email address" required>
        
        <p><?php echo $captcha ?></p>
        <input type="text" class="form-control" name="captcha" required>
        <a href="/login">Войти</a>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Зарегистрировать</button>
         <ul class="error-list">
        <?php if(!empty($errors)){
          foreach($errors as $error){
            echo "<li>----{$error}----</li>";
          }
        } ?>
      </ul>
      </form>


</div>
 <?php include ROOT.'/views/footer.php' ?>