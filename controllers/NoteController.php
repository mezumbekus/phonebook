<?php 

 class NoteController {


 	public function actionAdd() {
 		
 		$userfile = false;
 		if(!empty($_FILES['image'])){
 			$userfile = Helper::saveImage($_FILES['image']);
                
         }
 		if(!empty($_POST)) {

 			$loginId = Site::checkAuth();
 			$_POST['loginId'] = $loginId;
 			Note::addElement($_POST,$userfile);
 			
 			$list = Note::getListById($loginId);
 			include ROOT.'/views/template.php';
 		}
 		return null;
 	
 }

 	public function actionUpdate($noteId) {
 		
       	$file = Note::getFileByNoteId($noteId);         
 		if(!empty($_FILES['image']['name'])){
			$userfile = Helper::saveImage($_FILES['image']);
			
			Helper::removeFile($file);
 		}
 		else
 			$userfile = $file;
		if(!empty($_POST)) {
 			$loginId = Site::checkAuth();
 			$_POST['loginId'] = $loginId;
 			Note::editElement($_POST,$userfile,$noteId);
			$list = Note::getListById($loginId);
		}
 		include ROOT.'/views/template.php';
		return null;
	}

	public function actionDelete($noteId) {
		$file = Note::getFileByNoteId($noteId);
		if($file)
			Helper::removeFile($file);
		if(Note::deleteNoteById($noteId)){
			header('Location: /');
		}else{
			'Ошибка при удалении записи!';
		}
		return null;
	}
 }
