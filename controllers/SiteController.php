<?php

class SiteController {

	public function actionIndex() {
		$loginId = Site::checkAuth();
		if(isset($loginId))
			$list = Note::getListById($loginId,Site::NOTES_LIMIT);
		include ROOT.'/views/index.php';
		return null;
	}
	
	public function actionSort($type) {
		$loginId = Site::checkAuth();
		if(isset($loginId))
			$list = Note::getListSortBy($loginId,$type);
	
		include ROOT.'/views/template.php';
		return null;
	}

	public function actionLogin() {
		$errors = null;
		$loginId = null;
                if(!isset($_SESSION['captcha'])){
                    Helper::genCaptcha();
                    $captcha = Helper::getTextCaptcha();
                }
                else
                    $captcha = Helper::getTextCaptcha();
		if(isset($_POST['login']))
		{
                       
			list($login,$password,$captcha_answer) = array_values($_POST);
                        
			if(!Site::checkLogin($login))
				$errors[] = 'Ошибка аутентификации';
			if(!Site::checkPassword($password))
				$errors[] = 'Ошибка аутентификации';
                        if(!Helper::checkCaptcha($captcha_answer)){
                                $errors[] = 'Неверная Каптча';
                                Helper::genCaptcha();
                                $captcha = Helper::getTextCaptcha();
                        }
			if(empty($errors) && ($loginId = User::getIdByLogin($login,md5($password)))) {
				Site::setAuth($loginId);
                                unset($_SESSION['captcha']);
				header('Location: /');
			}
                        
		}
                
		include ROOT.'/views/login.php';
		return null;
	}

	public function actionRegister() {
		$errors = null;
		$loginId = null;
                if(!isset($_SESSION['captcha'])){
                    Helper::genCaptcha();
                    $captcha = Helper::getTextCaptcha();
                }
                else
                    $captcha = Helper::getTextCaptcha();
		if(isset($_POST['login']))
		{

			list($login,$password,$password2,$email,$captcha_answer) = array_values($_POST);
			if($password !== $password2)
				$errors[] = 'Пароли не совпадают';
			if(!Site::checkLogin($login))
				$errors[] = 'Ошибка в имени';
			if(User::checkLoginByName($login))
				$errors[] = 'Имя уже используется';
			if(!Site::checkPassword($password))
				$errors[] = 'Ошибка в пароле';
			if(!Site::checkEmail($email))
				$errors[] = 'Ошибка в email';
			if(User::checkEmail($email))
				$errors[] = 'Такой email уже используется';
                        if(!Helper::checkCaptcha($captcha_answer)){
                                $errors[] = 'Неверная Каптча';
                                Helper::genCaptcha();
                                $captcha = Helper::getTextCaptcha();
                        }
                        
			if(empty($errors))
				if(!User::registerUser($login,md5($password),$email))
					$errors[] = 'Произошла ошибка при регистрации, повторите попытку';
				else {
					$loginId = User::getIdByLogin($login,md5($password));
					Site::setAuth($loginId);
                                        unset($_SESSION['captcha']);
					header('Location: /');
				}
                   
		}
		include ROOT.'/views/register.php';
		return null;
	}

	public function actionLogout() {
		
		Site::unsetAuth();
		header('Location: /');
	}



	
}
