<?php 

class Helper {

	const UPLOAD_DIR = '/views/images/';
        const  CAPTCHA_QUESTIONS = [
              '1 Тысяча' => 1000,
              '2 Тысячи' => 2000,
              'Три тыщи' => 3000,
              'пятерка' => 5,
              'десятка' => 10,
              'Численное занчение туза в картах' => 11
              
            ];
	static public function saveImage($files) {
		if($files['size'] > 2097152)
			return false;
		$ext = array_pop(explode('.',basename($files['name'])));
		
		if(!strcasecmp(mime_content_type($files['tmp_name']),'image/gif') && (!strcasecmp($ext,'jpg') || !strcasecmp($ext,'png')))
			return false;
             
		$uploadfile =  self::getRandomName($ext);
		$uploadfilePath = ROOT.self::UPLOAD_DIR . $uploadfile;
               
		if(move_uploaded_file($files['tmp_name'], $uploadfilePath)){
			return $uploadfile;
                }
		return false;

	}

	static private function getRandomName($ext) {
		return date('Y-m-d-H-i-s').'-'.substr(md5(microtime()),0,10).'.'.$ext;
	}

	static public function removeFile($file) {
		if(file_exists(ROOT.self::UPLOAD_DIR.$file)){
			unlink(ROOT.self::UPLOAD_DIR.$file);
		}
	}
        
        static public function genCaptcha() {
           
            
           
          
            $save[0] = mt_rand(0, count(self::CAPTCHA_QUESTIONS)-1);
            $save[1] = mt_rand(0, count(self::CAPTCHA_QUESTIONS)-1);
            $save[2] = mt_rand(0, 1);
            
            
            $_SESSION['captcha'] = serialize($save);
            
        }
        
        static public function getTextCaptcha() {
              $save = unserialize($_SESSION['captcha']);
              $question_map = array_values(array_flip(self::CAPTCHA_QUESTIONS));
              $resultMessage = 'Сколько будет: ';
                switch($save[2]) {
                case 0:
                    $save[2] = 0;
                    $resultMessage .= $question_map[$save[0]] . ' плюс ' . $question_map[$save[1]];
                    break;
                case 1:
                    $save[2] = 1;
                    $resultMessage .= $question_map[$save[0]] . ' минус ' . $question_map[$save[1]];
                    break;
              
               
            }
            return $resultMessage;
        }
        
        static public function checkCaptcha($answer) {
           
            $save = unserialize($_SESSION['captcha']);
          
            $question_map = array_values(self::CAPTCHA_QUESTIONS);
            
            switch((int)$save[2]) {
                case 0:
                   
                    if(!(((int)$question_map[$save[0]] + (int)$question_map[$save[1]]) == $answer))
                        
                        return false;
                    break;
                case 1:
                    
                    if(!(((int)$question_map[$save[0]] - (int)$question_map[$save[1]]) == $answer))
                        return false;
                    break;
            
               
            }
            return true;
        }
        
        
      
}