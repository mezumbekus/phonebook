<?php

Class Db
{
	public static $db = null;
	
	public static function getConnection()
	{
		$conf = include(ROOT.'/config/db.php');
		list($host,$dbname,$user,$password,$driver,$port) = array_values($conf);
		$dsn = "{$driver}:host={$host};dbname={$dbname};port={$port};charset=UTF8";
		
		if(!self::$db){
			try
			{
			self::$db = new PDO($dsn,$user,$password,
					[
				    	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				    	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			        ]);
			}catch(PDOException $e){
		 		die($e->getMessage());
			}
		}
		return self::$db;
	}
}