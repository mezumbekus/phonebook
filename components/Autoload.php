<?php
spl_autoload_register(function($class){

	$paths = include ROOT.'/config/paths.php';

	foreach($paths as $path) {
		$path = ROOT.'/'.$path.'/'.$class.'.php';
		if(is_file($path))
		    include($path);
	}
});
