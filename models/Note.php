<?php 

class Note {


	static public function getListByID($loginId,$limit = 20) {
		$db = Db::getConnection();

		$sql = 'SELECT * FROM notes WHERE loginId = :loginId LIMIT ' . $limit;

		$stmt = $db->prepare($sql);
		$stmt->execute([':loginId' => $loginId]);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	static public function getListSortBy($loginId,$type) {
                switch ((int)$type) {
                case 1: 
                    $type = 'name';
                    break;
                case 2: 
                    $type = 'surname';
                    break;
                case 3:
                    $type = 'email';
                    break;
                }
		$db = Db::getConnection();
		$sql = 'SELECT * FROM notes WHERE loginId = :loginId ORDER BY '.$type;
		
		$stmt = $db->prepare($sql);
		$stmt->execute([':loginId' => $loginId]);
		 return $stmt->fetchAll(PDO::FETCH_ASSOC);

	}

	

	static public function addElement($data,$userfile) {
		$db = Db::getConnection();
		list($phone,$name,$surname,$comment,$email,$loginId) = array_values($data);
		if($userfile === false)
		 $userfile = ' ';
		 $sql = "INSERT INTO notes (name,surname,comment,image,phone,email,loginId) VALUES (:name,:surname,:comment,:image,:phone,:email,:loginId)";
		 $stmt = $db->prepare($sql);
		 return $stmt->execute(
		 	[
		 		':name' => $name,
		 	    ':surname' => $surname,
		 	    ':comment' => $comment,
		 	    ':image' => $userfile,
		 	    ':phone' => $phone,
		 	    ':email' => $email,
		 	    ':loginId' => $loginId
		 	]
		 );
	}
        static public function editElement($data,$userfile,$noteId) {
		$db = Db::getConnection();
		list($phone,$name,$surname,$comment,$email,$loginId) = array_values($data);
                
		if($userfile === false)
		 $userfile = ' ';
		$sql = "UPDATE notes SET name = :name,surname=:surname,comment=:comment,image=:image,phone=:phone,email=:email WHERE noteId = :noteId AND loginId = :loginId";
                
		 $stmt = $db->prepare($sql);
		 return $stmt->execute(
		 	[
		 	    ':name' => $name,
		 	    ':surname' => $surname,
		 	    ':comment' => $comment,
		 	    ':image' => $userfile,
		 	    ':phone' => $phone,
		 	    ':email' => $email,
		 	    ':loginId' => $loginId,
                            ':noteId' => $noteId
		 	]
		 );
	}
	static public function getLastInsertId($loginId) {
		$db = Db::getConnection();
		
		$sql = 'SELECT noteId FROM notes WHERE loginId = :loginId ORDER BY created desc LIMIT 1';
		$stmt = $db->prepare($sql);
		$stmt->execute([':loginId' => $loginId]);
		return $stmt->fetch(PDO::FETCH_ASSOC)['noteId'];
	} 
 	static public function getFileByNoteId($noteId) {
 		$db = Db::getConnection();
 		$sql = 'SELECT image FROM notes WHERE noteId = :noteId';
 		$stmt = $db->prepare($sql);
		$stmt->execute([':noteId' => $noteId]);
		return $stmt->fetch(PDO::FETCH_ASSOC)['image'];
 	}
	static public function deleteNoteById($noteId) {

		$db = Db::getConnection();
		$sql = 'DELETE FROM notes WHERE noteId = :noteId';

		$stmt = $db->prepare($sql);
		return $stmt->execute([':noteId' => $noteId]);

	}
}