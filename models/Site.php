<?php

class Site {
	
	const NOTES_LIMIT = 20;

	static public function checkAuth() {
		if(isset($_SESSION['login']))
			return $_SESSION['login'];
	}

	static public function setAuth($id) {
		$_SESSION['login'] = $id;
	}

	
	static public function unsetAuth() {
		unset($_SESSION['login']);
	}

	static public function checkLogin($login) {
		return preg_match('/^(?i:[a-z0-9]+)$/', $login);
	}

	static public function checkPassword($password) {
		return preg_match('/(?<=\d)[A-z]|(?<=[A-z])\d/', $password);
	}

	static public function checkEmail($email) {
		return preg_match('/^(?i:[\w|.]+@[\w]+\.\w+)$/', $email);
	}
}
