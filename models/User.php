<?php 

class User {
	
	

	static public function getIdByLogin($login,$password) {

		$db = Db::getConnection();

		$sql = 'SELECT loginId FROM users WHERE login = :login AND password = :password';

		$stmt = $db->prepare($sql);
		$stmt->execute([':login' => $login,':password' => $password]);
		return $stmt->fetch(PDO::FETCH_ASSOC)['loginId'];
	}

	static public function registerUser($login,$password,$email) {
		$db = Db::getConnection();
		$sql = 'INSERT INTO users (login,password,email) VALUES (:login,:password,:email)';
		$stmt = $db->prepare($sql);
		return $stmt->execute([':login' => $login,':password' => $password,':email' => $email]);

	}

	static public function checkLoginByName($name) {
		
		$db = Db::getConnection();
		$sql = 'SELECT count(*) as count FROM users WHERE login = :login';
		$stmt = $db->prepare($sql);
		$stmt->execute([':login' => $name]);
		return (bool)$stmt->fetch(PDO::FETCH_ASSOC)['count'];
	}

	static public function checkEmail($email) {
		
		$db = Db::getConnection();
		$sql = 'SELECT count(*) as count FROM users WHERE email = :email';
		$stmt = $db->prepare($sql);
		$stmt->execute([':email' => $email]);
		return (bool)$stmt->fetch(PDO::FETCH_ASSOC)['count'];
	}
}